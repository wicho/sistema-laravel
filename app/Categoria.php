<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //protected $table = 'categories';
    //protected $primaryKey = 'id';
    protected $fillable = ['name' , 'descripcion' , 'condicion'];

    public function productos()
    {
        return $this->hasMany('App/Producto');
    }
}
