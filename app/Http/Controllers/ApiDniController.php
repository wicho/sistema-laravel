<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

define('DNI_ENDPOINT', 'http://api.ateneaperu.com/api/reniec/DNI?sNroDocumento=');

class ApiDniController extends Controller
{
    public function index(Request $request, $dni)
    {
    
        return $this->get_ruc_info_from_dni($dni); 
            
    }

    public function get_ruc_info_from_dni($dni) {
        $dni = (string)$dni;
        $client = new \GuzzleHttp\Client();
    
        try {
            $res = $client->request('GET', DNI_ENDPOINT . $dni);
    
            $json_respone = $res->getBody();
            if ($res->getStatusCode() == 200) {
                return json_decode($json_respone, true);
            }
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    
        return FALSE;
    }
}
