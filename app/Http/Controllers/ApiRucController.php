<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

define('RUC_ENDPOINT', 'http://api.ateneaperu.com/api/Sunat/Ruc?sNroDocumento=');

class ApiRucController extends Controller
{
    public function index(Request $request, $ruc)
    {
    
        return $this->get_ruc_info_from_dni($ruc); 
            
    }

    public function get_ruc_info_from_dni($ruc) {
        $ruc = (string)$ruc;
        $client = new \GuzzleHttp\Client();
    
        try {
            $res = $client->request('GET', RUC_ENDPOINT . $ruc);
    
            $json_respone = $res->getBody();
            if ($res->getStatusCode() == 200) {
                return json_decode($json_respone, true);
            }
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    
        return FALSE;
    }
}
